package com.kshrd.dynamiclist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kshrd.dynamiclist.entity.Person;

import java.util.List;

/**
 * Created by pirang on 6/24/17.
 */

public class CustomAdapter extends BaseAdapter {

    List<Person> personList;

    public CustomAdapter(List<Person> personList) {
        this.personList = personList;
    }

    @Override
    public int getCount() {
        return personList.size();
    }

    @Override
    public Object getItem(int position) {
        return personList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_person, viewGroup, false);
        TextView tvId = (TextView) v.findViewById(R.id.tvId);
        TextView tvName = (TextView) v.findViewById(R.id.tvName);

        Person p = personList.get(position);
        tvId.setText(String.valueOf(p.getId()));
        tvName.setText(p.getName());

        return v;
    }
}
