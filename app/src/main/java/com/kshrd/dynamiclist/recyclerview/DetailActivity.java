package com.kshrd.dynamiclist.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.dynamiclist.MainActivity;
import com.kshrd.dynamiclist.R;
import com.kshrd.dynamiclist.entity.Article;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Article article = getIntent().getParcelableExtra(RecyclerViewActivity.ARTICLE_EXTRA);
        if (article != null) {

        }
    }
}
