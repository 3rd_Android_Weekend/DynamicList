package com.kshrd.dynamiclist.recyclerview;

/**
 * Created by pirang on 7/1/17.
 */

public interface RecyclerItemListener {

    void onItemClicked(int position);

}
