package com.kshrd.dynamiclist.recyclerview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kshrd.dynamiclist.R;
import com.kshrd.dynamiclist.entity.Article;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewActivity extends AppCompatActivity implements RecyclerItemListener {

    public static final String ARTICLE_EXTRA = "article";
    @BindView(R.id.rvArticle)
    RecyclerView rvArticle;

    private ArticleAdapter articleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        ButterKnife.bind(this);

        final List<Article> articleList = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            articleList.add(new Article (i, "Article -> " + i));
        }

        articleAdapter = new ArticleAdapter();
        articleAdapter.setRecyclerItemListener(this);
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        rvArticle.setAdapter(articleAdapter);
        articleAdapter.addMoreItem(articleList);


    }

    @Override
    public void onItemClicked(int position) {
        Article article = articleAdapter.getArticle(position);
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(ARTICLE_EXTRA, article);
        startActivity(intent);
    }
}
