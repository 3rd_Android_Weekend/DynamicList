package com.kshrd.dynamiclist.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kshrd.dynamiclist.R;
import com.kshrd.dynamiclist.entity.Article;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by pirang on 6/25/17.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder> {

    List<Article> articleList;
    RecyclerItemListener recyclerItemListener;

    public ArticleAdapter() {
        articleList = new ArrayList<>();
    }

    public void addMoreItem(List<Article> articles) {
        this.articleList.addAll(articles);
        notifyDataSetChanged();
    }

    public Article getArticle(int position){
        return articleList.get(position);
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_article, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        Article article = articleList.get(position);
        holder.tvId.setText(String.valueOf(article.getId()));
        holder.tvTitle.setText(article.getTitle());
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public void setRecyclerItemListener(RecyclerItemListener listener) {
        this.recyclerItemListener = listener;
    }

    class ArticleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvId)
        TextView tvId;

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        public ArticleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemListener.onItemClicked(getAdapterPosition());
                }
            });

        }

        @OnClick(R.id.tvTitle)
        void onTitleClicked() {
            Log.e("ooooo", articleList.get(getAdapterPosition()).getTitle());
        }
    }

}
